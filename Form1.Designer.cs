﻿namespace Cliente_FTP
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.UploadData = new System.Windows.Forms.Button();
            this.textLog = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.TextBox();
            this.Nombre = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.IniciarKPN = new System.Windows.Forms.Button();
            this.DetenerKPN = new System.Windows.Forms.Button();
            this.TestDownload = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UploadData
            // 
            this.UploadData.Location = new System.Drawing.Point(196, 31);
            this.UploadData.Name = "UploadData";
            this.UploadData.Size = new System.Drawing.Size(86, 28);
            this.UploadData.TabIndex = 0;
            this.UploadData.Text = "Subir Datos";
            this.UploadData.UseVisualStyleBackColor = true;
            this.UploadData.Click += new System.EventHandler(this.UploadData_Click);
            // 
            // textLog
            // 
            this.textLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLog.BackColor = System.Drawing.Color.White;
            this.textLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textLog.Location = new System.Drawing.Point(452, 12);
            this.textLog.Multiline = true;
            this.textLog.Name = "textLog";
            this.textLog.ReadOnly = true;
            this.textLog.Size = new System.Drawing.Size(561, 348);
            this.textLog.TabIndex = 1;
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(287, 39);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(128, 20);
            this.textName.TabIndex = 2;
            // 
            // Nombre
            // 
            this.Nombre.AutoSize = true;
            this.Nombre.BackColor = System.Drawing.Color.Transparent;
            this.Nombre.Location = new System.Drawing.Point(284, 23);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(58, 13);
            this.Nombre.TabIndex = 3;
            this.Nombre.Text = "Tu nombre";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // IniciarKPN
            // 
            this.IniciarKPN.Enabled = false;
            this.IniciarKPN.Location = new System.Drawing.Point(12, 31);
            this.IniciarKPN.Name = "IniciarKPN";
            this.IniciarKPN.Size = new System.Drawing.Size(86, 28);
            this.IniciarKPN.TabIndex = 4;
            this.IniciarKPN.Text = "Iniciar KPN";
            this.IniciarKPN.UseVisualStyleBackColor = true;
            this.IniciarKPN.Click += new System.EventHandler(this.IniciarKPN_Click);
            // 
            // DetenerKPN
            // 
            this.DetenerKPN.Enabled = false;
            this.DetenerKPN.Location = new System.Drawing.Point(104, 31);
            this.DetenerKPN.Name = "DetenerKPN";
            this.DetenerKPN.Size = new System.Drawing.Size(86, 28);
            this.DetenerKPN.TabIndex = 5;
            this.DetenerKPN.Text = "Detener KPN";
            this.DetenerKPN.UseVisualStyleBackColor = true;
            this.DetenerKPN.Click += new System.EventHandler(this.DetenerKPN_Click);
            // 
            // TestDownload
            // 
            this.TestDownload.AutoSize = true;
            this.TestDownload.BackColor = System.Drawing.Color.Transparent;
            this.TestDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestDownload.Location = new System.Drawing.Point(12, 82);
            this.TestDownload.Name = "TestDownload";
            this.TestDownload.Size = new System.Drawing.Size(128, 20);
            this.TestDownload.TabIndex = 8;
            this.TestDownload.Text = "Test Download";
            this.TestDownload.Click += new System.EventHandler(this.TestDownload_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 372);
            this.Controls.Add(this.TestDownload);
            this.Controls.Add(this.DetenerKPN);
            this.Controls.Add(this.IniciarKPN);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.textLog);
            this.Controls.Add(this.UploadData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Launcher & Updater";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown_1);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove_1);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button UploadData;
        private System.Windows.Forms.TextBox textLog;
        private System.Windows.Forms.Label Nombre;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button IniciarKPN;
        private System.Windows.Forms.Button DetenerKPN;
        private System.Windows.Forms.Label TestDownload;
    }
}

